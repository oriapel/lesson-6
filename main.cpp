#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "Hexagon.h"
#include "Pentagon.h"
int main()
{
	std::string nam, col, rad = "0", ang = "0", height = "0", width = "0", st, shapetype, ang2, fin;
	Circle circ(col, nam, 0);
	quadrilateral quad(nam, col, 0, 0);
	rectangle rec(nam, col, 0, 0);
	parallelogram para(nam, col, 0, 0, 0, 180);
	Hexagon hex(col, nam, 0);
	Pentagon pent(col, nam, 0);
	InputException ex("0");
	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para; 
	std::string::size_type sz;
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p';
	char x = 'y';
	while (x != 'x') 
	{
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, t = pentagon" << std::endl;
		std::cin >> shapetype;
		try
		{
			if (shapetype.length() == 1)
			{
				switch (shapetype[0]) {
				case 'c':
					std::cout << "enter color, name,  rad for circle" << std::endl;
					std::cin >> col >> nam >> rad;
					circ.setColor(col);
					circ.setName(nam);
					try
					{
						ex.setInput(rad);
						ex.what();
						circ.setRad(std::stod(rad, &sz));
						ptrcirc->draw();
					}

					catch (const std::string e)
					{
						std::cerr << e;
					}
					break;
				case 'q':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					quad.setName(nam);
					quad.setColor(col);
					try
					{
						ex.setInput(height); ex.what();
						quad.setHeight(std::stod(height, &sz));
						ex.setInput(width); ex.what();
						quad.setWidth(std::stod(width, &sz));
						ptrquad->draw();
						throw InputException(rad);
					}

					catch (std::string s)
					{
						std::cerr << s;
					}
					break;
				case 'r':
					std::cout << "enter name, color, height, width" << std::endl;
					std::cin >> nam >> col >> height >> width;
					rec.setName(nam);
					rec.setColor(col);
					try
					{
						ex.setInput(height); ex.what();
						rec.setHeight(std::stod(height, &sz));
						ex.setInput(width); ex.what();
						rec.setWidth(std::stod(width, &sz));
						ptrrec->draw();
					}

					catch (std::string s)
					{
						std::cerr << s;
					}

					break;
				case 'p':
					std::cout << "enter name, color, height, width, 2 angles" << std::endl;
					std::cin >> nam >> col >> height >> width >> ang >> ang2;
					para.setName(nam);
					para.setColor(col);
					try
					{
						ex.setInput(height); ex.what();
						para.setHeight(std::stod(height, &sz));
						ex.setInput(width); ex.what();
						para.setWidth(std::stod(width, &sz));
						ex.setInput(ang); ex.what();
						ex.setInput(ang2); ex.what();
						para.setAngle(std::stod(ang, &sz), std::stod(ang2, &sz));
						try
						{
							if (ang == ang2 || ((ang != "0" || ang != "180") && (ang2 != "0" && ang2 != "180")))
								throw shapeException();
							else
								ptrpara->draw();
						}

						catch (std::exception& error)
						{
							std::cerr << "ERROR " << error.what();
						}
					}

					catch (std::string s)
					{
						std::cerr << s;
					}
					break;
				case 'h':
					std::cout << "enter name, color, fin" << std::endl;
					std::cin >> nam >> col >> fin;
					hex.setName(nam);
					hex.setColor(col);
					try
					{
						ex.setInput(fin); ex.what();
						hex.setFin(std::stod(fin, &sz));
						hex.draw();
					}
					catch (std::string s)
					{
						std::cerr << s;
					}
					break;
				case 't':
					std::cout << "enter name, color, fin" << std::endl;
					std::cin >> nam >> col >> fin;
					pent.setName(nam);
					pent.setColor(col);
					try
					{
						ex.setInput(fin); ex.what();
						pent.setFin(std::stod(fin, &sz));
						pent.draw();
					}
					catch (std::string s)
					{
						std::cerr << s;
					}
				case 'x':
					break;
				default:
					std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
					break;
				}
			}

			else if((shapetype.find("c") || shapetype.find("q") || shapetype.find("r") || shapetype.find("p") || shapetype.find("t") || shapetype.find("h")) && shapetype.length() > 1)
			{
				std::cout << "Warning - Don't try to build more than one shape at once" << std::endl;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin >> x;
		}
			catch (std::exception e)
			{			
				printf(e.what());
			}
			catch (...)
			{
				printf("caught a bad exception. continuing as usual\n");
			}
	}



		system("pause");
		return 0;
	
}
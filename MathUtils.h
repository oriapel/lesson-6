#pragma once
class MathUtils
{
public:
	MathUtils(double fin);
	static double CalPentagonArea(double fin);
	static double CalHexagonArea(double fin);
private:
	double _fin;
};

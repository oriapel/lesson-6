#pragma once
#include "shape.h"
#include "MathUtils.h"
class Pentagon:public Shape
{
private:
	double _fin;
public:
	Pentagon(std::string col, std::string nam, double fin);
	void draw(); 
	double CalArea();
	void setFin(double fin);
};
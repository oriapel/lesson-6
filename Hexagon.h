#pragma once
#include "shape.h"
#include "MathUtils.h"

class Hexagon:public Shape
{
public:
	Hexagon(std::string col, std::string nam, double fin);
	void setFin(double Fin);
	void draw();
	double CalArea();
private:
	double _fin;
};
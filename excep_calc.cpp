#include <iostream>
#include <string>
#define ILLEGAL_NUM 8200
int add(int a, int b) {
	if (a == ILLEGAL_NUM || b == ILLEGAL_NUM || (a + b) == ILLEGAL_NUM)
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year");
  return a + b;
}

int  multiply(int a, int b) 
{
  int sum = 0;
  for(int i = 0; i < b; i++)
  {
    sum = add(sum, a);
	if (a == ILLEGAL_NUM || b == ILLEGAL_NUM || sum == ILLEGAL_NUM)
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year");
  };
  return sum;
}

int  pow(int a, int b) 
{
  int exponent = 1;
  for(int i = 0; i < b; i++) 
  {
    exponent = multiply(exponent, a);
	if (a == ILLEGAL_NUM || b == ILLEGAL_NUM || exponent == ILLEGAL_NUM)
		throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year");
  }
  return exponent;
}

int main(void)
{
	std::cout << "8200 ^ 2";
  try{
	std::cout <<" = " << pow(8200, 2) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }
  std::cout << "2 ^ 6";
  try {
	  std::cout << " = " << pow(2, 6) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }
  std::cout << " 4100 * 2";
  try {
	  std::cout << " = " << multiply(4100, 2) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }

  std::cout << "5 * 2";
  try {
	  std::cout << " = " << multiply(5, 2) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }

  std::cout << "8200 + 1";
  try {
	  std::cout << " = " << add(8200, 1) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }

  std::cout << "5 + 1";
  try {
	  std::cout << " = " << add(5, 1) << std::endl;
  }

  catch (std::string s) {
	  std::cerr << "\n" << s << std::endl;
  }
  getchar();
  return 0;
}
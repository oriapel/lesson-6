#include "MathUtils.h"
MathUtils::MathUtils(double fin):_fin(fin)
{}
double MathUtils::CalPentagonArea(double fin)
{
	return 1.72048 * fin * fin;
}

double MathUtils::CalHexagonArea(double fin)
{
	return 2.59808 * fin * fin;
}
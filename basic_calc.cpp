#include <iostream>
#define ILLEGALL true
#define ILLEGALL_NUM 8200
using namespace std;

int add(int a, int b, bool& is_data_ilegall)
{
	is_data_ilegall = a == ILLEGALL_NUM || b == ILLEGALL_NUM || (a + b) == ILLEGALL_NUM;
	return a + b;
}

int  multiply(int a, int b, bool& is_data_ilegall)
{
	int sum = 0;
	for (int i = 0; i < b; i++) 
	{
		sum = add(sum, a, is_data_ilegall);
		is_data_ilegall = a == ILLEGALL_NUM || b == ILLEGALL_NUM || is_data_ilegall || sum == ILLEGALL_NUM;
	}
	return sum;
}

int pow(int a, int b, bool& is_data_ilegall)
{
	int exponent = 1;
	int* ans = new int[2];
	for (int i = 0; i < b; i++) 
	{
		exponent = multiply(exponent, a, is_data_ilegall);
		is_data_ilegall = a == ILLEGALL_NUM || b == ILLEGALL_NUM || is_data_ilegall;
	};
	return exponent;
}

/*
This function is printing the answer of the calculation
*/
void printData(bool& illegall, int ans)
{
	if (illegall)
	{
		cout << endl;
		cerr<<("This user is not authorized to access 8200, please enter different numbers, or try to getclearance in 1 year")<<endl;
		illegall = false;
	}
	else
		cout << " = " << ans << endl;

}

int main(void)
{
	bool is_data_ilegall = false;
	int ans;
	cout << "trying to pow invallid numbers 8200 ^ 5";
	ans = pow(8200, 5, is_data_ilegall);
	printData(is_data_ilegall, ans);
	cout << "trying to pow vallid numbers 3 ^ 1";
	ans = pow(3, 1, is_data_ilegall);
	printData(is_data_ilegall, ans);
	cout << "multiply vallid numvers 6 * 8";
	ans = multiply(6, 8, is_data_ilegall);
	printData(is_data_ilegall, ans);
	cout << "multiply invallid numvers 82 * 100";
	ans = multiply(82, 100, is_data_ilegall);
	printData(is_data_ilegall, ans);
	cout << "add vallid numbers 1 + 600";
	ans = add(1, 600, is_data_ilegall);
	printData(is_data_ilegall, ans);
	cout << "add invallid numbers 8200 + 55";
	ans = add(8200, 55, is_data_ilegall);
	printData(is_data_ilegall, ans);
	getchar();
	return 0;
}
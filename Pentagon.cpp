#include "Pentagon.h"

Pentagon::Pentagon(std::string nam, std::string col, double fin) :Shape(nam, col)
{
	setFin(fin);
}
void Pentagon::setFin(double fin)
{
	_fin = fin;
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "fin length is " << _fin << std::endl << "Area is: " << CalArea()<<std::endl;
}

double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(_fin);
}